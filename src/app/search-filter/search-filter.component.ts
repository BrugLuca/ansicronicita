import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter, OnDestroy } from '@angular/core';
import { fromEvent, of, throwError, Subscription } from 'rxjs';
import { map, debounceTime, catchError, distinctUntilChanged } from 'rxjs/operators'


@Component({
  selector: 'app-search-filter',
  templateUrl: './search-filter.component.html',
  styleUrls: ['./search-filter.component.css']
})
export class SearchFilterComponent implements OnInit,OnDestroy {

  constructor() { }



  @ViewChild('filterBar') Filtro: ElementRef; //per pcollegare un elmento del html con qualcosa in ts

  @Output() emmettitoreString = new EventEmitter<string>();

  subscription:Subscription;

  ngOnInit() {

    var input$ = fromEvent(this.Filtro.nativeElement, 'keyup'); //creo un observable
    var error$ = throwError("this is an error");

    this.subscription = input$.pipe(
      debounceTime(200), //ritardo di 200 ms per ridurre numero chiamate
      map((eventoIn: any) => eventoIn.target.value),
      distinctUntilChanged(), //ridurre chiamate per modifiche al campo di imìnput che riporta a una stringa già vista.
      catchError(err => {
        console.log('With catchError: ', err)
        return of([])  // messo percheè se l'errore è presente voglio tornare all situazione iniziale
      }
      )).subscribe(inputString => this.emmettitoreString.emit(inputString)); //sottoscrivo l'ibservable con una subsription

    //usiamo il map per trasformare il risultato, che sarebbe un oggeto con tutte le info di premimento, uso il map per convertirlo solo nel valore del tasto premuto.

  }

  ngOnDestroy(): void{
    this.subscription.unsubscribe(); //così chiudo la sub e libero memoria

  }
}
