import { Component } from '@angular/core';
import { Observable, of } from 'rxjs';
import { MockHeroes } from './_models/mock-heroes';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'asicronicita / Observable exercies';
  
  heroes:string[]=MockHeroes;
  filteredH : string[] = this.heroes.slice();
  filtrodiricerca(evento:string) {
    if(!evento) { //la stringa è falsa quando è nulla, vuota o undefined.
      this.heroes=this.filteredH;
    } else {  
      this.heroes=this.filteredH.filter(element => element.toUpperCase().includes(evento.toUpperCase())); 
      //il fatto che sia o meno case sensitive dipende da includes, il metteere "toUpperCase" 
      //permette di pasasre entrambe le stringhe, quelle in ingresso e quelle che verifica in upper case
      //questo evita che ci sia un case sensitive, perché è tutto upper.
       }
    }
  
 
  constructor(){
  //  this.chiamante("luca", this.salutoClassico); 
  //  this.chiamante("luca", this.salutoAlternativo); 
  /*  console.log(1);
     const osservante = new Observable(observer => {
       console.log('obs1');
       setTimeout(() => {
         console.log('obs 2');
         observer.next('hello');
        }, 5000);});
        
        console.log(2);
        
        osservante.subscribe( calue => console.log(calue)); 
      console.log(3);
         
        of('ciao').subscribe(x=> console.log(x));
        const observabile = new Observable(observer =>{
          observer.next('1');
          observer.next('2');
          observer.error('error')
          observer.next('3');
          observer.complete();
        });

        observabile.subscribe(value => console.log(value),
        error => console.log(error),
        () => console.log('completed')
      );
  }

   salutoClassico() {console.log('Ciao');}
   salutoAlternativo() { console.log('yo');}
  
   chiamante(nome, callbackSaluto) {
      callbackSaluto();
      console.log(nome);
      callbackSaluto();

*/

  }

  



}


