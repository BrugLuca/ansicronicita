
function salutoClassico() {console.log('Ciao');}
function salutoAlternativo() { console.log('yo');}

function chiamante(nome, callbackSaluto) {
    callbackSaluto();
    console.log(nome);
    callbackSaluto();
}

chiamante("luca", salutoClassico);